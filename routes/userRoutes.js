const {Router} = require('express');
const UserService = require('../services/userService');
const {createUserValid, updateUserValid} = require('../middlewares/user.validation.middleware');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

router.get('/', function (req, res) {
  const result = UserService.getAll();
  if ( result.length ) {
    res.status(200).send(result)
  } else {
    const errorResponse = {
      error: true,
      message: 'Users not found'
    };
    res.status(400).send(errorResponse);
  }
});

router.get('/:id', function (req, res) {
  if (req && req.params && req.params.id) {
    const searchBy = {id: req.params.id};
    const result = UserService.search(searchBy);
    if (result) {
      res.send(result);
    } else {
      res.status(404).send(`User with id:${searchBy.id} not found`);
    }
  } else {
    const errorResponse = {
      error: true,
      message: `Param ':id' not found`
    };
    res.status(400).send(errorResponse);
  }
});


router.post('/', createUserValid, function (req, res) {
  const newUser = UserService.create(req.body);
  if ( newUser ) {
    res.status(201).send(`User with id:${newUser.id} created. You can now use credentials to sing in.`)
  } else {
    const errorResponse = {
      error: true,
      message: 'No data to use.'
    };
    res.status(400).send(errorResponse);
  }
});

router.put('/:id', updateUserValid, function (req, res) {
  if ( req && req.params && req.params.id ) {
    const dataToUpdate = req.body;
    const result = UserService.update(req.params.id, dataToUpdate);
    if ( result ) {
      res.status(202).send(`User ${result.firstName} updated successfully.`);
    } else {
      const errorResponse = {
        error: true,
        message: `No user found with id: ${req.params.id}`
      };
      res.status(404).send(errorResponse);
    }
  } else {
    const errorResponse = {
      error: true,
      message: 'No data to work with.'
    };
    res.status(404).send(errorResponse);
  }
});

router.delete('/:id', function (req, res) {
  if ( req && req.params && req.params.id ) {
    const data = {id: req.params.id};
    const result = UserService.delete(data);
    if ( result && result.length ) {
      res.status(200).send(`User w/id:${req.params.id} deleted successfully.`);
    } else {
      const errorResponse = {
        error: true,
        message: `User w/id:${req.params.id} not found.`
      };
      res.status(404).send(errorResponse);
    }
  } else {
    const errorResponse = {
      error: true,
      message: 'No data to work with.'
    };
    res.status(400).send(errorResponse);
  }
});


module.exports = router;

const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();
// TODO: Implement route controllers for fighter

// endpoints
router.get('/', function (req, res) {
  const result = FighterService.getAll();
  if (result.length) {
    res.status(200).send(result)
  } else {
    const errorResponse = {
      error: true,
      message: 'Fighters not found'
    };
    res.status(400).send(errorResponse);
  }
});

router.get('/:id', function (req, res) {
  if (req && req.params && req.params.id) {
    const searchBy = {id: req.params.id};
    const result = FighterService.search(searchBy);
    if (result) {
      res.send(result);
    } else {
      res.status(404).send(`Fighter with id:${searchBy.id} not found`);
    }
  } else {
    const errorResponse = {
      error: true,
      message: `Param ':id' not found`
    };
    res.status(400).send(errorResponse);
  }
});

router.post('/', createFighterValid, function (req, res) {
  const newFighter = FighterService.create(req.body);
  if ( newFighter ) {
    res.status(201).send(`User with id:${newFighter.id} created.`)
  } else {
    const errorResponse = {
      error: true,
      message: 'No data to use.'
    };
    res.status(400).send(errorResponse);
  }
});

router.put('/:id', updateFighterValid, function (req, res) {
  if ( req && req.params && req.params.id ) {
    const dataToUpdate = req.body;
    const result = FighterService.update(req.params.id, dataToUpdate);
    if ( result ) {
      res.status(202).send(`Fighter ${result.firstName} updated successfully.`);
    } else {
      const errorResponse = {
        error: true,
        message: `No fighter found with id: ${req.params.id}`
      };
      res.status(404).send(errorResponse);
    }
  } else {
    const errorResponse = {
      error: true,
      message: 'No data to work with.'
    };
    res.status(404).send(errorResponse);
  }
});

router.delete('/:id', function (req, res) {
  if (req && req.params && req.params.id) {
    const fighter = {id: req.params.id};
    const result = FighterService.delete(fighter);
    if (result && result.length) {
      res.status(200).send(`Fighter w/id:${req.params.id} deleted successfully.`);
    } else {
      const errorResponse = {
        error: true,
        message: `Fighter w/id:${req.params.id} not found.`
      };
      res.status(404).send(errorResponse);
    }
  } else {
    const errorResponse = {
      error: true,
      message: 'No data to work with.'
    };
    res.status(400).send(errorResponse);
  }
});

module.exports = router;

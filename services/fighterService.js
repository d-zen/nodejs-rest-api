const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {

  search (search) {
    const item = FighterRepository.getOne(search);
    // return item ? item : null;
    if (!item) {
      return null;
    }
    return item;
  }

  create (fighter) {
    if (fighter) {
      return FighterRepository.create(fighter);
    }
  }

  update(id, fighter) {
    return FighterRepository.update(id, fighter);
  }

  delete(fighter) {
    return FighterRepository.delete(fighter.id);
  }

  getAll() {
    return FighterRepository.getAll();
  }

}

module.exports = new FighterService();

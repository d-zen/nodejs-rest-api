const { UserRepository } = require('../repositories/userRepository');

class UserService {

  // TODO: Implement methods to work with user

  search(search) {
    const item = UserRepository.getOne(search);
    if(!item) {
      return null;
    }
    return item;
  }

  delete ( user ) {
    return UserRepository.delete(user.id)
  }

  create (user) {
    const existing = this.search(user);
    return existing ? false : UserRepository.create(user);
  }

  update (id, user) {
    return UserRepository.update(id, user)
  }

  getAll() {
    return UserRepository.getAll();
  }

}

module.exports = new UserService();

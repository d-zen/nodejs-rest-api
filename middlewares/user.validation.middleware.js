const {user} = require('../models/user');
const UserService = require('../services/userService');
const isEmpty = require('lodash.isempty');

function isValidGoogleMail(mail) {
  return isValidEmail(mail) && isGoogleMail(mail);
}

function isValidEmail(email) {
  return email && /\S+@\S+\.\S+/.test(email);
}

function isGoogleMail(email) {
  return email.endsWith('@gmail.com')
}


function isValidNumber(phoneNumber) {
  return phoneNumber && phoneNumber.length === 13 && phoneNumber.startsWith('+380');
}

function hasAllFields(data) {
  Object.keys(user).forEach(key => {
    if ( key !== 'id' ) {
      if ( !data.hasOwnProperty(key) || isString(data[key]) ) {
        return false;
      }
    }
  });
  return true;
}

function isString(val) {
  return ( typeof val === 'string' || val instanceof String )
}

function validData(data) {
  let validation = true;

  if ( !data ) {
    validation = false;
  }

  if ( Object.keys(data).length < Object.keys(user).length ) {
    validation = false;
  }

  if ( !hasAllFields(data) ) {
    validation = false;
  }
  debugger
  return validation;
}

function isValidPassword(password) {
  return password && password.length >= 3;
}


const createUserValid = (req, res, next) => {
  const err = {};
  if ( req && req.body && !isEmpty(req.body) ) {
    if ( validData(req.body) ) {
      const {firstName, lastName, email, phoneNumber, password} = req.body;

      const existing = UserService.search({firstName, lastName, email, phoneNumber});
      if ( existing ) {
        err.error = true;
        err.message = `User already exists. Created at ${existing.createdAt}`;
        res.status(400).send(err);
      }
      if ( firstName && lastName && phoneNumber && email && password ) {
        if ( !isValidNumber(phoneNumber) ) {
          err.error = true;
          err.message = 'Invalid phone number.';
          res.status(400).send(err);
          return
        }

        if ( !isValidGoogleMail(email) ) {
          err.error = true;
          err.message = 'Invalid email.';
          res.status(400).send(err);
          return
        }

        if ( !isValidPassword(password) ) {
          err.error = true;
          err.message = 'Invalid password.';
          res.status(400).send(err);
          return
        }
        next();
      } else {
        if ( !isEmpty(err) ) {
          err.error = true;
          err.message = 'Some error.';
          res.status(400).send(err);
        } else {
          next();
        }
      }
    } else {
      err.error = true;
      err.message = 'Missing required fields';
      res.status(400).send(err);
    }
  } else {
    err.error = true;
    err.message = 'No data to use';
    res.status(400).send(err);
  }
};

const updateUserValid = (req, res, next) => {
  const err = {};
  // if (null == req.body || 'object' != typeof req.body) {
  if ( req.params && req.params.id ) {
    if ( req && req.body && !isEmpty(req.body) ) {
      const {firstName, lastName, email, phoneNumber, password} = req.body;

      Object.keys(req.body).forEach(key => {
        if ( !Object.keys(user).includes(key) ) {
          err.error = true;
          err.message = 'Unsupported property';
          res.status(400).send(err);
        } else {
          switch (key) {
            case 'email':
              if ( !isValidGoogleMail(email) ) {
                err.error = true;
                err.message = 'Invalid email.';
                res.status(400).send(err);
                return
              }
              break;
            case 'phoneNumber':
              if ( !isValidNumber(phoneNumber) ) {
                err.error = true;
                err.message = 'Invalid phone number.';
                res.status(400).send(err);
                return
              }
              break;
            case 'password':
              if ( !isValidPassword(password) ) {
                err.error = true;
                err.message = 'Invalid password.';
                res.status(400).send(err);
                return
              }
          }
        }
      });
      next();
    } else {
      err.error = true;
      err.message = 'No update data to use';
      res.status(400).send(err);
    }
  } else {
    err.error = true;
    err.message = 'param /:id not found';
    res.status(400).send(err);
  }
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;

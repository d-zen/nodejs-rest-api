const {fighter} = require('../models/fighter');
const isEmpty = require('lodash.isempty');
const FighterService = require('../services/fighterService');

function isValidPower(power) {
  return isNumber(power) && ( power > 0 && power <= 100 );
}

function isValidDefense(def) {
  return isNumber(def) && ( def > 0 && def <= 100 );
}

function isValidHealth(health) {
  return isNumber(health) && ( health > 0 && health <= 100 );
}

function hasAllFields(data) {
  Object.keys(fighter).forEach(key => {
    if ( key !== 'id' ) {
      if ( !data.hasOwnProperty(key) || isString(data[key]) ) {
        return false;
      }
    }
  });
  return true;
}

function isString(val) {
  return ( typeof val === 'string' || val instanceof String )
}

function isNumber(val) {
  return ( typeof val === 'number' || val instanceof Number )
}

function validData(data) {
  let validation = true;
  if ( !data ) {
    validation = false;
  }
  if ( Object.keys(data).length < (Object.keys(fighter).length - 1)) {
    validation = false;
  }

  if ( !hasAllFields(data) ) {
    validation = false;
  }
  return validation;
}

const createFighterValid = (req, res, next) => {
  const err = {};
  if ( req && req.body && !isEmpty(req.body) ) {
    if (Object.keys(req.body).includes('id')) {
      err.error = true;
      err.message = `param Id is redundant`;
      res.status(400).send(err);
      return;
    }
    if ( validData(req.body) ) {
      const {name, health, power, defense, source} = req.body;
      const existing = FighterService.search(req.body);
      if ( existing ) {
        err.error = true;
        err.message = `Fighter already exists. Created at ${existing.createdAt}`;
        res.status(400).send(err);
      }
      if (name && health && power && defense ) {

        if ( !isValidHealth(health) ) {
          err.error = true;
          err.message = 'Invalid health';
          res.status(400).send(err);
          return
        }

        if ( !isValidPower(power) ) {
          err.error = true;
          err.message = 'Invalid power.';
          res.status(400).send(err);
          return
        }

        if ( !isValidDefense(defense) ) {
          err.error = true;
          err.message = 'Invalid defence.';
          res.status(400).send(err);
          return
        }
        next();
      } else {
        err.error = true;
        err.message = 'Missing required fields';
        res.status(400).send(err);
      }
    } else {
      err.error = true;
      err.message = 'Missing required fields';
      res.status(400).send(err);
    }
  } else {
    err.error = true;
    err.message = 'No data to use';
    res.status(400).send(err);
  }
};

const updateFighterValid = (req, res, next) => {
  const err = {};
  if ( req.params && req.params.id ) {
    if ( req && req.body && !isEmpty(req.body) ) {
      const {name, health, power, defense} = req.body;

      Object.keys(req.body).forEach(key => {
        if ( !Object.keys(fighter).includes(key) ) {
          err.error = true;
          err.message = `Unsupported property: ${key}`;
          res.status(400).send(err);
        } else {
          switch (key) {
            case 'health':
              if ( !isValidHealth(health) ) {
                err.error = true;
                err.message = 'Invalid health.';
                res.status(400).send(err);
                return
              }
              break;
            case 'power':
              if ( !isValidPower(power) ) {
                err.error = true;
                err.message = 'Invalid power.';
                res.status(400).send(err);
                return
              }
              break;
            case 'defence':
              if ( !isValidDefense(defense) ) {
                err.error = true;
                err.message = 'Invalid defence.';
                res.status(400).send(err);
                return
              }
          }
        }
      });
      next();
    } else {
      err.error = true;
      err.message = 'No update data to use';
      res.status(400).send(err);
    }
  } else {
    err.error = true;
    err.message = 'param /:id not found';
    res.status(400).send(err);
  }
};


exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;

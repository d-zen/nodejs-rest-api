import { get, post, put, deleteReq } from "../requestHelper";

const entity = 'fighters';

export const getFighters = async () => {
    return await get(entity);
}

export const createFighter = async (body) => {
    return await post(entity, body);
}

export const editFighter = async (body) => {
    return await put(entity, body);
}

export const deleteFighter = async (body) => {
    return await deleteReq(entity, body);
}

import { deleteReq, get, post, put } from "../requestHelper";

const entity = 'fights';

export const getFights = async () => {
  return await get(entity);
};

export const createFight = async (body) => {
  return await post(entity, body);
};

export const editFight = async (body) => {
  return await put(entity, body);
};

export const deleteFight = async (body) => {
  return await deleteReq(entity, body);
};

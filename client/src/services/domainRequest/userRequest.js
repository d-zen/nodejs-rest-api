import { deleteReq, post, put, get } from "../requestHelper";

const entity = 'users';

export const getUser = async (body) => {
  return await get(entity, body);
};

export const createUser = async (body) => {
  return await post(entity, body);
};

export const editUser = async (body) => {
  return await put(entity, body);
};

export const deleteUser = async (body) => {
  return await deleteReq(entity, body);
};
